<!DOCTYPE html>
<html lang="en">
<?php
    if(is_file('configDB.local.php')){
        include_once ("configDB.local.php");
    } else {
        include_once("configDB.php");
    }
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BLOG</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <div class="accueil">
            <h1><a href="accueil.php">Accueil</a></h1>
        </div>
        <h1>BLOG</h1>
        <?php
            if(!isset($_SESSION["user"])){
                header("Location: connexion.php");
                exit(0);
                /*echo '<form action="connexion.php">';
                echo '<button type="submit">Se connecter</button>';
                echo '</form>';*/
            }
            else{
                ?>
                <div class="user-profile-dropdown">
                    <img src="<?php
                        // Mise à jour de la photo avant chargement
                        if(!empty($_FILES["photoprofil"])){
                            $nouveauChemin = "./imagewebp/user_".$_SESSION["user"]->id."/userprofile.png";
                            if(!is_dir("./imagewebp/user_".$_SESSION["user"]->id)){ // Si le dossier n'existe pas, on le crée
                                mkdir("./imagewebp/user_".$_SESSION["user"]->id);
                            }
                            Move_uploaded_file($_FILES["photoprofil"]["tmp_name"], $nouveauChemin); // Déplacer le dossier
                            $requeteMAJphoto = "UPDATE utilisateur SET photoprofil = '".$nouveauChemin."' WHERE id = ".$_SESSION["user"]->id;
                            $connexion->query($requeteMAJphoto);
                        }

                        //Affichage d'une photo de profil
                        $requeteLienPhoto = $connexion->query("SELECT photoprofil FROM utilisateur WHERE id = ".$_SESSION["user"]->id);
                        $lien = $requeteLienPhoto->fetch();
                        if($lien["photoprofil"] == "" || !file_exists($lien["photoprofil"])){ // Si le fichier a été supprimé du serveur
                            $requeteMAJphoto = "UPDATE utilisateur SET photoprofil = NULL WHERE id = ".$_SESSION["user"]->id;
                            $connexion->query($requeteMAJphoto);
                            $requeteLienPhoto = $connexion->query("SELECT photoprofil FROM utilisateur WHERE id = ".$_SESSION["user"]->id);
                            $lien = $requeteLienPhoto->fetch();
                            echo "imagewebp\User-Profile-PNG-Image.png";
                        }
                        else{
                            echo $lien["photoprofil"];
                        }
                    ?>" alt="User profile" id="profile-image">
                    <div class="dropdown-content">
                        <form action="newarticle.php">
                            <button type="submit">Créer article</button>
                        </form>
                        <form action="profil.php">
                            <button type="submit">Mon profil</button>
                        </form>
                        <?php
                        if($_SESSION["user"]->admin == 1){
                            ?>
                            <form action="admin.php">
                                <button type="submit">Paramètres</button>
                            </form>
                            <?php
                        }?>
                        <form id="logout-form" method="POST">
                            <button type="submit" form="logout-form">Se déconnecter</button>
                            <input name="hidden" type="hidden" value="Deconnexion">
                        </form>
                    </div>
                </div>
                <?php
                if(isset($_POST["hidden"]) && $_POST["hidden"]="Deconnexion"){
                    unset($_SESSION["user"]);
                    $pagecourante=basename($_SERVER["PHP_SELF"]);
                    header("Location: $pagecourante");
                    exit(0);
                }
            }
        ?>
    </header>