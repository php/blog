
<?php
include_once ('header.php');
?>
    <main>
        <h1>Bienvenue sur Blog</h1>
        <?php
            $requete ="SELECT DISTINCT a.id, a.titre, a.description, /*c.nom,*/ u.pseudo FROM article a 
            INNER JOIN utilisateur u ON a.iduser = u.id 
            LEFT JOIN categorie_article ca ON a.id = ca.idarticle
            LEFT JOIN categorie c ON ca.idcategorie = c.id
            ORDER BY a.id DESC";


            if (isset($_GET["auteur"]) && !empty($_GET["auteur"])) {
                $auteur = '%' . $_GET["auteur"] . '%';
                $requete = "SELECT DISTINCT a.id, a.titre, a.description, u.pseudo 
                        FROM article a 
                        INNER JOIN utilisateur u ON a.iduser = u.id 
                        LEFT JOIN categorie_article ca ON a.id = ca.idarticle
                        LEFT JOIN categorie c ON ca.idcategorie = c.id
                        WHERE u.pseudo LIKE :auteur
                        ORDER BY a.id DESC";
            }

            $requeteArticles = $connexion->prepare($requete);
            if (isset($auteur)) {
                $requeteArticles->bindParam(':auteur', $auteur, PDO::PARAM_STR);
            }
            $requeteArticles->execute();

            echo "<h2>Articles à la une :</h2>";?>
            <form method="GET" action="accueil.php">
                <label for="auteur">Filtrer par auteur :</label>
                <input type="text" id="auteur" name="auteur" placeholder="Nom de l'auteur">
                <button type="submit">Filtrer</button>
            </form>

            <br>
            <?php
            echo '<form method="GET" action="article.php">';
            echo "<table id='listarticleaccueil'>";
            echo "<tr>";
            echo "<th>Titre de l'article</th>";
            echo "<th>Catégorie</th>";
            echo "<th>Auteur</th>";
            echo "</tr>";
            $nbarticle=$requeteArticles->rowCount();
            if($nbarticle>0){
                /*
                if($nbarticle>10){
                    $nbarticle=10;
                }
                */
                for($i = 0; $i < $nbarticle; $i++){
                    $ligne=$requeteArticles->fetch();

                    // Récupération des différentes catégories associées à l'article
                    $requeteRecuperationCategorie = $connexion->prepare("SELECT ca.idarticle, c.nom AS nomCategorie FROM categorie_article AS ca
                    LEFT JOIN categorie c ON ca.idcategorie = c.id
                    WHERE ca.idarticle = ".$ligne["id"]."
                    ");
                    $requeteRecuperationCategorie->execute();
                    $nbCategories = $requeteRecuperationCategorie->rowCount();
                    if($nbCategories == 0){
                        $categories = "Aucune";
                    }
                    else{
                        $lignecategorie=$requeteRecuperationCategorie->fetch();
                        $categories = $lignecategorie["nomCategorie"];
                        for($j = 1; $j < $nbCategories; $j++){
                            $lignecategorie=$requeteRecuperationCategorie->fetch();
                            $categories = $categories."/".$lignecategorie["nomCategorie"];
                        }
                    }

                    // Affichage des articles
                    echo '<tr>';
                    echo '<td><button name="selectArticle" value="'.$ligne["id"].'">'.$ligne["titre"].'</button></td>';
                    echo "<td><p>".$categories."</p></td>";
                    echo "<td><p>".$ligne["pseudo"]."</p></td>";
                    echo "</tr>";
                }
            }
            echo "</table>";
            echo "</form>";
        ?>
    </main>
<?php
    include_once ('footer.php');
?>