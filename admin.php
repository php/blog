<?php
    include_once ('header.php');
    if($_SESSION["user"]->admin != 1){
        header("Location: accueil.php");
    }
?>
    <main>
        
        <form method="POST">
            <?php
            // Ajouter une catégorie
            if(isset($_POST["ajouterCategorie"]) && $_POST["ajouterCategorie"]!=""){
                $estDajaDansLatable = false;
                $requeteCategories = $connexion->query("SELECT * FROM categorie");
                foreach($requeteCategories AS $lignecategories){
                    if($lignecategories["nom"] == $_POST["ajouterCategorie"]){
                        $estDajaDansLatable = true;
                    }
                }
                if(!$estDajaDansLatable){
                    $requeteAjouterCategorie = $connexion->query("INSERT INTO categorie(nom) VALUES('".$_POST["ajouterCategorie"]."')");
                    echo '<p>La catégorie '.$_POST["ajouterCategorie"].' a bien été ajoutée.</p>';
                }
                else{
                    echo "<p>La catégorie existe déjà.</p>";
                }
                
            }
            
            // Supprimer une catégorie
            if(isset($_POST["supprimerCategorie"])){
                $estPasSupprime = false;
                $requeteCategories = $connexion->query("SELECT * FROM categorie");
                foreach($requeteCategories AS $lignecategories){
                    if($lignecategories["id"] == $_POST["supprimerCategorie"]){
                        $estPasSupprime = true;
                    }
                }
                if($estPasSupprime){

                    // Supprimer la catégorie-article
                    $requeteSupprimerCategorieArticle = "DELETE FROM categorie_article WHERE idcategorie = :categorie_id";
                    $requeteSupprimerCategorieArticle = $connexion->prepare($requeteSupprimerCategorieArticle);
                    $requeteSupprimerCategorieArticle->bindParam(':categorie_id', $_POST["supprimerCategorie"], PDO::PARAM_INT);
                    $requeteSupprimerCategorieArticle->execute();
        
                    // Supprimer la catégorie
                    $requeteSupprimerCategorie = "DELETE FROM categorie WHERE id = :categorie_id";
                    $requeteSupprimerCategorie = $connexion->prepare($requeteSupprimerCategorie);
                    $requeteSupprimerCategorie->bindParam(':categorie_id', $_POST["supprimerCategorie"], PDO::PARAM_INT);
                    $requeteSupprimerCategorie->execute();

                    echo '<p>L'."'".'article a bien été supprimée.</p>';
                }
            }

            $requeteCategories = $connexion->query("SELECT * FROM categorie"); // Mise à jour après une insertion
            ?>
            <h2>Ajouter une catégorie</h2>
            <label for="ajouterCategorie">Nom de la catégorie : </label>
            <input type="text" name="ajouterCategorie">
            <button type="submit">Ajouter</button>
            <?php
            echo '<h2>Catégories existantes</h2>';
            foreach($requeteCategories AS $lignecategories){
                echo '<div>';
                echo '<label for="'.$lignecategories["id"].'">'.$lignecategories["nom"].'</label>';
                echo '<button name="supprimerCategorie" value="'.$lignecategories["id"].'">Supprimer</button>';
                echo '</div>';
            }
            ?>
        </form>
    </main>
<?php
    include_once ('footer.php');
?>