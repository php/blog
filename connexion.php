<!DOCTYPE html>
<html lang="en">
<?php
    if(is_file('configDB.local.php')){
        include_once ("configDB.local.php");
    } else {
        include_once("configDB.php");
    }
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BLOG</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <header>
        <h1>BLOG</h1>
    </header>
    <main id="connexion">
        <h2>Connexion</h2>
        <form method="post">
            <label for="email">
                E-mail
            </label>
            <?php
                if(!empty($_POST["email"])){
                    echo "<input type='email' id='email' name='email' required placeholder='Adresse e-mail' value='" . $_POST["email"] . "'>";
                }
                else{
                    echo '<input type="email" id="email" name="email" required placeholder="Adresse e-mail">';
                }
            ?>
            <label for="mdp">
                Mot de passe
            </label>
            <input id="mdp" type="password" name="mdp" required placeholder="Mot de passe" pattern="^(?=.*[0-9]).{1,}$">
            <button type="submit">
                Valider
            </button>
        </form>
        <?php
            if(!empty($_POST['email']) && !empty($_POST['mdp'])) {
                $mdpvalide=false;
                $charobligatoire=['1','2','3','4','5','6','7','8','9','0'];
                foreach($charobligatoire as $char){
                    if(in_array($char,str_split($_POST["mdp"],1))){
                        $mdpvalide=true;
                    }
                }
                $charinvalide=['"','(','{','[',']','|','\\',')','}','/',';',"'",':'];
                foreach($charinvalide as $char){
                    if(in_array($char,str_split($_POST["mdp"],1))){
                        $mdpvalide=false;
                    }
                }
                if($mdpvalide==true){
                    $requete=$connexion->prepare("SELECT * from utilisateur where email=?");
                    $requete->bindParam(1,$_POST["email"]);
                    $requete->execute();
                    if($requete->rowCount()==0){
                        $tmpname="user";
                        for($i=0;$i<9;$i++){
                            $tmpname=$tmpname . random_int(0,9);
                        }
                        $requete=$connexion->prepare("INSERT INTO utilisateur (email,mdp,pseudo,admin) values (?,?,?,0)");
                        $requete->bindParam(1,$_POST["email"]);
                        $requete->bindParam(2,$_POST["mdp"]);
                        $requete->bindParam(3,$tmpname);
                        $requete->execute();
                        $requete=$connexion->prepare("SELECT id from utilisateur where email=?");
                        $requete->bindParam(1,$_POST["email"]);
                        $requete->execute();
                        $tmpid=$requete->fetch();
                        $_SESSION["user"]=new User($tmpname,$_POST["email"],$_POST["mdp"],0,$tmpid["id"]);
                        header("Location: accueil.php");
                        exit(0);
                    }
                    else if($requete->rowCount()==1){
                        $requete=$requete->fetch();
                        if($requete["mdp"]==$_POST["mdp"]){
                            $_SESSION["user"]=new User($requete["pseudo"],$requete["email"],$requete["mdp"],$requete["admin"],$requete["id"]);
                            header("Location: accueil.php");
                            exit(0);
                        }
                        else{
                            echo '<p class="erreurMdp">Mot de passe incorrect<p>';
                        }
                    }
                    else{
                    }
                }  
            }
        ?>
    </main>
    <?php
        include_once ('footer.php');
    ?>
</body>