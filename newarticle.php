<?php
    include_once ('header.php');
?>
    <main>
        <form id="creationarticle" method="POST">
            <section>
                <label for="titre">Titre de l'article</label>
                <input type="text" id="titre" name="titre" placeholder="Titre" maxlength="255" required>
            </section>
            <section>
                <label for="description">Corps de l'article</label>
                <textarea id="description" name="description" placholder="Corps de l'article" required maxlength="20000" cols="100" rows="10"></textarea>
            </section>
            <?php
                $requeteCategorie=$connexion->prepare("SELECT * FROM CATEGORIE");
                $requeteCategorie->execute();
                if($requeteCategorie->rowCount()>0){
                    echo '<section><label for="categoriecheck">Catégorie(s) :</label>';
                    echo '<fieldset name="categoriecheck" id="categoriecheck">';
                    for($i=0;$i<$requeteCategorie->rowCount();$i++){
                        $ligne=$requeteCategorie->fetch();
                        echo '<input type="checkbox" id="'.$ligne["nom"].'" name="categorie[]" value="'.$ligne["id"].'">';
                        echo '<label for="'.$ligne["nom"].'">'.$ligne["nom"].'</label>';
                        echo '<br>';

                    }
                    echo '</fieldset></section>';

                }
            ?>
            <button type="submit">Confirmer</button>
        </form>
        <?php
            if((!empty($_POST["titre"])&&strlen($_POST["titre"])<=255)&&!empty($_POST["description"])){
                $requeteArticle=$connexion->prepare("INSERT INTO ARTICLE(titre,description,iduser) VALUES (?,?,?)");
                $requeteArticle->bindParam(1,$_POST["titre"]);
                $requeteArticle->bindParam(2,$_POST["description"]);
                $requeteArticle->bindParam(3,$_SESSION["user"]->id);
                $requeteArticle->execute();
                if(!empty($_POST["categorie"])){
                    foreach($_POST["categorie"] as $cat){
                        $requeteCategorie=$connexion->prepare("INSERT INTO CATEGORIE_ARTICLE VALUES (?,?)");
                        $idarticle=$connexion->prepare("SELECT max(id) as idarticle from article where iduser=?");
                        $idarticle->bindParam(1,$_SESSION["user"]->id);
                        $idarticle->execute();
                        $idart=$idarticle->fetch();
                        $requeteCategorie->bindParam(1,$idart["idarticle"]);
                        $requeteCategorie->bindParam(2,$cat);
                        $requeteCategorie->execute();
                    }
                }
                header("Location: newarticleconfirmation.php");
                exit(0);
            }
        ?>
    </main>
<?php
    include_once ('footer.php');
?>