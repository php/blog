<?php
    include_once ('header.php');
    if(!empty($_POST["pseudo"]) && !empty($_POST["newmdp"])){
        if($_POST["newmdp"]!=$_SESSION["user"]->mdp){
            $mdpvalide=false;
            $charobligatoire=['1','2','3','4','5','6','7','8','9','0'];
            foreach($charobligatoire as $char){
                if(in_array($char,str_split($_POST["newmdp"],1))){
                    $mdpvalide=true;
                }
            }
            $charinvalide=['"','(','{','[',']','|','\\',')','}','/',';',"'",':'];
            foreach($charinvalide as $char){
                if(in_array($char,str_split($_POST["newmdp"],1))){
                    $mdpvalide=false;
                }
            }
        }
        if($_POST["newmdp"]!=$_SESSION["user"]->mdp && $mdpvalide==true){
            $requetemdp=$connexion->prepare("UPDATE utilisateur SET mdp=? WHERE id=?");
            $requetemdp->bindParam(1,$_POST["newmdp"]);
            $requetemdp->bindParam(2,$_SESSION["user"]->id);
            $requetemdp->execute();
            $_SESSION["user"]->mdp=$_POST["newmdp"];
        }
        if($_POST["pseudo"]!=$_SESSION["user"]->nom){
            $requetepseudo=$connexion->prepare("UPDATE utilisateur SET pseudo=? WHERE id=?");
            $requetepseudo->bindParam(1,$_POST["pseudo"]);
            $requetepseudo->bindParam(2,$_SESSION["user"]->id);
            $requetepseudo->execute();
            $_SESSION["user"]->nom=$_POST["pseudo"];
        }

        

        header("Location: profil.php");
        exit(0);
    }
?>
<h1>Mon profil</h1>
<form method="post" id="profil" enctype="multipart/form-data">
    <fieldset>
        <p>Photo</p>
        <img src="<?php
                        //Affichage d'une photo de profil
                        $requeteLienPhoto = $connexion->query("SELECT photoprofil FROM utilisateur WHERE id = ".$_SESSION["user"]->id);
                        $lien = $requeteLienPhoto->fetch();
                        if($lien["photoprofil"] != ""){
                            echo $lien["photoprofil"];
                        }
                        else{
                            echo "imagewebp\User-Profile-PNG-Image.png";
                        }
                    ?>" alt="User profile" id="profile-image">
        <input type="file" name="photoprofil" accept=".png, .jpg">
    </fieldset>
    <fieldset>
        <label for="pseudo">Pseudo</label>
        <input required type="text" id="pseudo" name="pseudo" value="<?php if(!empty($_POST["pseudo"]) && $_POST["pseudo"]!=$_SESSION["user"]->nom){
                                                                        echo $_POST["pseudo"];
                                                                    }
                                                                    else{
                                                                        echo $_SESSION["user"]->nom;
                                                                    }?>">
        <label for="newmdp">Mot de passe</label>
        <input required placeholder="Mot de passe" type="text" id="newmdp" name="newmdp" value="<?php echo $_SESSION["user"]->mdp;?>" pattern="^(?=.*[0-9]).{1,}$">
    </fieldset>
    <button type="submit">Confirmer les changements</button>
</form>
<?php
    include_once ('footer.php');
?> 