<?php
    class User{
        public $nom;
        public $mail;
        public $mdp;
        public $admin;
        public $id;

        function __construct($nom,$mail,$mdp,$admin,$id){
            $this->nom=$nom;
            $this->mail=$mail;
            $this->mdp=$mdp;
            $this->admin=$admin;
            $this->id=$id;
        }

        public function estAdmin(){
            return $admin==1;
        }

        public function changeMDP($newmdp){
            $this->admin=$newmdp;
        }
    }

    session_start();

    try{
        $chemin='localhost';
        $nom_base_de_donnee='blog_php';
        $identifiant='root';
        $mot_de_passe='';

        $pdo="mysql:host=".$chemin.";dbname=".$nom_base_de_donnee;
        $connexion = new PDO($pdo, $identifiant, $mot_de_passe);
        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOExeption $e){
        die('Erreur PDO : ' . $e->getMessage());
    }
    catch(Exeption $e){
        die('Erreur Générale : ' . $e->getMessage());
    }

?>