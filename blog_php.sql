-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mar. 10 oct. 2023 à 19:49
-- Version du serveur : 5.6.20-log
-- Version de PHP : 8.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `blog_php`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `description` text,
  `iduser` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id`, `titre`, `description`, `iduser`) VALUES
(33, 'Accéder au bureau à distance de l\'IUT', 'Pour accéder au bureau à distance de l\'IUT :\r\nOuvrez le menu Démarrer -> Tous les programmes -> Accessoires -> Connexion bureau à distance  (ou parfois Accessoires -> Communication -> ...)\r\nLa boîte de dialogue « Connexion bureau à distance » apparaît\r\nTapez tseetu.univ-lyon1.fr dans le champ « Ordinateur », puis cliquez sur le bouton « Connexion »\r\nIndiquez votre compte étudiant sous la forme univ-lyon1\\p0123456, ainsi que votre mot de passe habituel\r\nUne fois connecté au serveur distant, vous pouvez retrouver les applications pédagogiques dans le menu Démarrer\r\nVous pouvez à tout moment retrouver le bureau de votre ordinateur local en fermant le bandeau jaune ou bleu tout en haut de l\'écran (icône « _ »)\r\n\r\n', 10),
(32, 'Antoine Dupont', 'J\'ai appris que Antoine Dupont va pouvoir reprendre les matchs de rugby du côté de France. Son opération s\'est bien passé, on espère pouvoir remporter cette coupe du monde de rugby 2023.', 10),
(34, 'le temps des tempêtes', 'nicolas sarkozy', 11),
(35, 'necronomicon', 'Crée par H.P. Lovecraft, est un ouvrage fictif repris par la suite dans plusieurs œuvres littéraires.', 11),
(36, 'Max Verstappen, chamion du monde 2023', 'Suite à sa victoire lors du Qatar GP, Max verstappen est sacré champion du monde', 12),
(37, 'Victoire de Yannick Noah', 'Yannick Noah s\'impose en 2 sets', 12);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `nom` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom`) VALUES
(4, 'Sport'),
(3, 'Informatique'),
(5, 'Littérature'),
(6, 'Travail');

-- --------------------------------------------------------

--
-- Structure de la table `categorie_article`
--

CREATE TABLE `categorie_article` (
  `idarticle` int(11) NOT NULL,
  `idcategorie` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categorie_article`
--

INSERT INTO `categorie_article` (`idarticle`, `idcategorie`) VALUES
(32, 4),
(33, 3),
(33, 6),
(35, 5),
(36, 4),
(37, 4);

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE `commentaire` (
  `id` int(11) NOT NULL,
  `description` text,
  `iduser` int(11) DEFAULT NULL,
  `idarticle` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`id`, `description`, `iduser`, `idarticle`) VALUES
(14, 'Dommage qu il n y ait pas beaucoup de logiciels intéressants...', 13, 33),
(13, 'Le menu Démarrer du bureau à distance ne fonctionne pas.', 12, 33),
(12, 'Super !', 11, 32),
(16, 'Trop fort !', 12, 37);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mdp` varchar(255) DEFAULT NULL,
  `pseudo` varchar(25) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  `photoprofil` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `email`, `mdp`, `pseudo`, `admin`, `photoprofil`) VALUES
(13, 'mehdi.bourbon@etu.univ-lyon1.fr', 'mezdu12', 'Mehdi', 0, NULL),
(12, 'yacine.bouanani@etu.univ-lyon1.fr', 'Yacine69', 'Yacine', 0, NULL),
(11, 'hugo.mayrand@etu.univ-lyon1.fr', 'Hugo69', 'Hugo', 0, NULL),
(10, 'loric.audin@etu.univ-lyon1.fr', 'NeRegardePasMonCode69', 'Loric', 0, NULL),
(1, 'admin@localhost.fr', 'admin69IUT', 'admin', 1, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `categorie_article`
--
ALTER TABLE `categorie_article`
  ADD PRIMARY KEY (`idarticle`,`idcategorie`),
  ADD KEY `idcategorie` (`idcategorie`);

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `commentaire`
--
ALTER TABLE `commentaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
