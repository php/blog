<?php
    include_once ('header.php');
?>
<?php
    if(isset($_GET["selectArticle"])){
        // Récupérer l'article
        $requeteArticle = "SELECT a.id, a.titre, a.description, a.iduser, u.pseudo from article AS a JOIN utilisateur AS u ON a.iduser = u.id WHERE a.id = ".$_GET["selectArticle"];
        $rechercheArticle = $connexion->prepare($requeteArticle);
        $rechercheArticle->execute();
        // Ajouter un nouveau commentaire
        if(isset($_POST["nouveau_commentaire"]) && (empty($_SESSION["nouveau_commentaire"]) || $_POST["nouveau_commentaire"] != $_SESSION["nouveau_commentaire"])){ // On vérifie si un commentaire a été ajouté et on fait attention à le pas mettre plusieurs fois le même commentaire au rechargement de la session
            $_SESSION["nouveau_commentaire"] = $_POST["nouveau_commentaire"];

            $nouveauCommentaire = "INSERT INTO commentaire (description, iduser, idarticle) VALUES ('".$_SESSION["nouveau_commentaire"]."', ".$_SESSION["user"]->id.", ".$_GET["selectArticle"].")";
            $requeteAjouterCommentaire = $connexion->prepare($nouveauCommentaire);
            $requeteAjouterCommentaire->execute();
            
        }
        else if(empty($_POST["nouveau_commentaire"])){
            unset($_SESSION["nouveau_commentaire"]);
        }

        // Supprimer un commentaire
        if(isset($_POST["commentaire_a_supprimer"])){
            $commentaireASupprimer = "DELETE FROM commentaire WHERE id = ".$_POST["commentaire_a_supprimer"];
            $requeteSupprimerCommentaire = $connexion->prepare($commentaireASupprimer);
            $requeteSupprimerCommentaire->execute();
        }

        // Supprimer un article et ses commentaires
        if (isset($_POST["article_id"])) {
            $article_id = $_POST["article_id"];

            // Supprimer les commentaires liés à l'article
            $requeteSupprimerCommentaires = "DELETE FROM commentaire WHERE idarticle = :article_id";
            $requeteSupprimerCommentaires = $connexion->prepare($requeteSupprimerCommentaires);
            $requeteSupprimerCommentaires->bindParam(':article_id', $article_id, PDO::PARAM_INT);
            $requeteSupprimerCommentaires->execute();

            // Supprimer la catégorie-article
            $requeteSupprimerCategorieArticle = "DELETE FROM categorie_article WHERE idarticle = :article_id";
            $requeteSupprimerCategorieArticle = $connexion->prepare($requeteSupprimerCategorieArticle);
            $requeteSupprimerCategorieArticle->bindParam(':article_id', $article_id, PDO::PARAM_INT);
            $requeteSupprimerCategorieArticle->execute();

            // Supprimer l'article
            $requeteSupprimerArticle = "DELETE FROM article WHERE id = :article_id";
            $requeteSupprimerArticle = $connexion->prepare($requeteSupprimerArticle);
            $requeteSupprimerArticle->bindParam(':article_id', $article_id, PDO::PARAM_INT);
            $requeteSupprimerArticle->execute();

            // Rediriger vers la page d'accueil
            header("Location: supprimer_article.php");
        }


        // Gestion des commentaires
        if($rechercheArticle->rowCount() == 1){
            $articleAAfficher = $rechercheArticle->fetch();
            $requeteCommentaire = "SELECT c.id, c.description, c.iduser, c.idarticle, u.pseudo from commentaire AS c JOIN utilisateur u ON c.iduser = u.id WHERE c.idarticle = ".$_GET["selectArticle"]." ORDER BY c.id DESC";
?>

            <h2><?php echo $articleAAfficher["titre"]; ?></h2>
            <h3>Auteur : <?php echo $articleAAfficher["pseudo"]; ?></h3>
            <article>
                <section>
                    <?php echo $articleAAfficher["description"]; ?>
                </section>

                <?php
                // Afficher le bouton de suppression de l'article pour le propriétaire
                if(isset($_SESSION["user"]) && ($_SESSION["user"]->id == $articleAAfficher["iduser"] || $_SESSION["user"]->admin == 1)) {
                    echo '<form method="POST">';
                    echo '<input type="hidden" name="article_id" value="'.$articleAAfficher["id"].'">';
                    echo '<button type="submit" name="submit">Supprimer cet article</button>';
                    echo '</form>';
                }
                ?>

                <form method="POST">
                    <label for="nouveau_commentaire"><h2>Commenter l'article</h2></label>
                    <textarea id="nouveau_commentaire" name="nouveau_commentaire"></textarea>
                    <button type="submit">Envoyer</button>
                </form>

                <?php
                echo '<h2>Commentaires</h2>';
                foreach($connexion->query($requeteCommentaire) as $commentaire) {
                    echo '<section>';
                    echo '<h3>'.$commentaire["pseudo"].'</h3>';
                    echo ''.$commentaire["description"].'';

                    // Afficher le bouton de suppression de commentaire pour le propriétaire
                    if (isset($_SESSION["user"]) && ($_SESSION["user"]->id == $commentaire["iduser"] || $_SESSION["user"]->admin == 1)) {
                        echo '<form method="POST">';
                        echo '<input type="hidden" name="commentaire_a_supprimer" value="'.$commentaire["id"].'">';
                        echo '<button type="submit" name="submit">Supprimer ce commentaire</button>';
                        echo '</form>';
                    }

                    echo '</section>';
                }
                ?>
            </article>

    <?php
            }
            else{
                echo '<h2>Erreur</h2>';
                echo '<p>La page que vous recherchez est introuvable.</p>';
                echo '<form action="accueil.php">';
                echo "<button>Retour à l'accueil</button>";
                echo '</form>';
            }                    
        }
        else{
            echo '<h2>Erreur</h2>';
            echo '<p>Aucun article n'."'".'a été sélectionné.</p>';
            echo '<form action="accueil.php">';
            echo "<button>Retour à l'accueil</button>";
            echo '</form>';
        }
        
    ?>
    <?php
        include_once ('footer.php');
    ?>